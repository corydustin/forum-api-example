# Forum Api Example

Example REST API to handle interactions based on forums with posts and comments on those posts. 

## Getting started
To get started with the project, utilize the following steps to get up and running! 
1. Clone project from repo
2. If you don't already have the latest Java JDK, download JDK 18 from the following link: https://www.oracle.com/java/technologies/downloads/
3. Make sure you have your JAVA_HOME env variable setup within your machine (either Windows or Mac). If you need help setting this env variable up, please see the following links:
   1. Windows: https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html 
   2. Mac: https://stackoverflow.com/questions/22842743/how-to-set-java-home-environment-variable-on-mac-os-x-10-9
4. From the root of the project, run the following command from your terminal or IntelliJ terminal (if you decided to open the project there): 
`./gradlew bootRun`
5. With the server running the following can be done: 
   1. Navigate to the DB's UI via: http://localhost:8080/v1/forum/h2-console/
   2. Swagger Documentation Generated and can be view via: http://localhost:8080/v1/forum/swagger-ui/index.html
      1. Raw JSON of Swagger is located at: http://localhost:8080/v1/forum/v3/api-docs
   3. The following requests can be made via Postman or your favorite XHR request platform:
      1. `POST http://localhost:8080/v1/forum/posts` - Creates a new post
      2. `GET http://localhost:8080/v1/forum/posts/` - Retrieve all posts within DB
      3. `GET http://localhost:8080/v1/forum/posts/{id}` - Retrieves a post with `{id}`
      4. `POST http://localhost:8080/v1/forum/posts/{postId}/comments` - Creates a new comment for the post provided
      5. `GET http://localhost:8080/v1/forum/posts/{postId}/comments` - Retrieves all comments within the DB for a specific post
      6. `GET http://localhost:8080/v1/forum/posts/{postId/comments/{commentId}` - Retrieves a comment for the Post and Comment specified.
