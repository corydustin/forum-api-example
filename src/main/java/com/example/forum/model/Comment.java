package com.example.forum.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "COMMENT")
@Data
public class Comment extends RepresentationModel<Comment> {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @SequenceGenerator(name="commentSeqGen")
    @JsonIgnore
    @Column private long id;
    @Column @JsonIgnore @CreatedDate private LocalDateTime createdAt;
    @Column @JsonIgnore @LastModifiedDate private LocalDateTime updateAt;
    @Column @JsonIgnore private String createdBy;
    @Column @NotNull private String body;
    @ManyToOne(cascade = {CascadeType.ALL}) @JsonIgnore private Post post;

}
