package com.example.forum.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "POST")
@Data
public class Post extends RepresentationModel<Post> {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @SequenceGenerator(name="postSeqGen")
    @Column
    @JsonIgnore
    private long id;
    @CreatedDate @Column @JsonIgnore private LocalDateTime createdAt;
    @LastModifiedDate @Column @JsonIgnore private LocalDateTime updatedAt;
    @Column @NotBlank private String title;
    @Column @NotBlank private String body;
    @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true) private List<Comment> comments;

}
