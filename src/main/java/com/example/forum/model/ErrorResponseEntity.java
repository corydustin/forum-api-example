package com.example.forum.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
public class ErrorResponseEntity {

    private String message;
    private int code;
    private HttpStatus status;

}
