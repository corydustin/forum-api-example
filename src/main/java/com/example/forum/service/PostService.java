package com.example.forum.service;

import com.example.forum.model.Comment;
import com.example.forum.model.Post;
import com.example.forum.repository.CommentRepository;
import com.example.forum.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PostService {

    private PostRepository postRepository;
    private CommentRepository commentRepository;

    @Autowired
    public PostService(PostRepository postRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    public Post getPost(long id) {
        return this.postRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("No post found for id: " + id));
    }

    public List<Post> getAllPosts() {
        return this.postRepository.findAll(PageRequest.of(0, 20)).toList();
    }

    public Post savePost(Post post) {
        return this.postRepository.save(post);
    }

    public Post updatePost(Post post) {
        this.getPost(post.getId());
        return this.postRepository.save(post);
    }

    public void deletePost(Long id) {
        this.postRepository.deleteById(id);
    }

    public Comment getComment(long id) {
        return this.commentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("No comment found for id: " + id));
    }

    public List<Comment> getAllCommentsForPost(long id) {
        return this.postRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("No post found for id: " + id)).getComments();
    }

    public Comment saveComment(long postId, Comment comment) {
        // Determine if there is a Post already stored that we are attempting to save a Comment for
        Post post = this.postRepository.findById(postId).orElseThrow(() -> new EntityNotFoundException("No post found for id: " + postId));
        post.getComments().add(comment);
        this.postRepository.save(post);

        comment.setPost(post);
        return this.commentRepository.save(comment);
    }

    public Comment updateComment(long postId, Comment comment) {
        Comment existingComment = this.getComment(comment.getId());

        if(existingComment != null) {
            return this.commentRepository.save(comment);
        } else {
            throw new EntityNotFoundException("No comment found for post with id: " + postId);
        }

    }

    public void deleteComment(Long postId, Long commentId) {
        this.commentRepository.deleteById(commentId);
    }

}
