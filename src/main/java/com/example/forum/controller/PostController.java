package com.example.forum.controller;

import com.example.forum.model.Comment;
import com.example.forum.model.Post;
import com.example.forum.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.util.List;

/**
 * Rest Controller responsible for all requests related to the /posts endpoint and sub-resources /
 */
@RestController
@RequestMapping(value = PostController.RESOURCE_URL)
public class PostController {

    public static final String RESOURCE_URL = "/posts";

    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/")
    public ResponseEntity<Post> createPost(@Valid @RequestBody Post post) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.postService.savePost(post));
    }

    @GetMapping("/")
    public ResponseEntity<List<Post>> getAllPosts() {
        return ResponseEntity.ok(this.postService.getAllPosts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Post> getPost(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.postService.getPost(id).add(Link.of(String.format("%s/%s", RESOURCE_URL, id))));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Post> updatePost(@PathVariable("id") Long id, @Valid @RequestBody Post post) {
        post.setId(id);
        return ResponseEntity.ok(this.postService.updatePost(post));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePost(@PathVariable("id") Long id) {
        this.postService.deletePost(id);
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/{id}/comments")
    public ResponseEntity<Comment> createComment(@PathVariable("id") Long postId, @Valid @RequestBody Comment comment) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.postService.saveComment(postId, comment));
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public ResponseEntity<Comment> getComment(@PathVariable("postId") Long postId, @PathVariable("commentId") Long commentId) {
        // Setup comment with HATEOAS self link
        Comment comment = this.postService.getComment(commentId).add(Link.of(String.format("%s/%s/comments/%s", RESOURCE_URL, postId, commentId)));

        return ResponseEntity.ok(comment);
    }

    @PatchMapping("/{postId}/comments/{commentId}")
    public ResponseEntity<Comment> updateComment(@PathVariable("postId") Long postId,
                                                 @PathVariable("commentId") Long commentId,
                                                 @Valid @RequestBody Comment comment) {
        comment.setId(commentId);
        return ResponseEntity.ok(this.postService.updateComment(postId, comment));
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public ResponseEntity deleteComment(@PathVariable("postId") Long postId, @PathVariable("commentId") Long commentId) {

        Comment comment = this.postService.getComment(commentId);
        this.postService.deleteComment(postId, commentId);
        return ResponseEntity.accepted().build();
    }

    @GetMapping("/{id}/comments")
    public ResponseEntity<List<Comment>> getAllCommentsForPost(@PathVariable("id") Long id) {
       return ResponseEntity.ok(this.postService.getAllCommentsForPost(id));
    }

}
