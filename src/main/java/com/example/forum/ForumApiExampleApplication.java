package com.example.forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.forum.repository")
@EnableJpaAuditing
public class ForumApiExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForumApiExampleApplication.class, args);
	}

}
